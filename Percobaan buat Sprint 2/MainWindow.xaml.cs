﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Percobaan_buat_Sprint_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btn_berechnen_Click(object sender, RoutedEventArgs e)
        {
            /*if (String.IsNullOrEmpty(tb_kopfdiameter.Text) | String.IsNullOrEmpty(tb_silinderdiameter.Text) | String.IsNullOrEmpty(tb_silinderhoehe.Text) | String.IsNullOrEmpty(tb_kopfhoehe.Text) | String.IsNullOrEmpty(tb_dichte.Text))
            {
                MessageBox.Show("Bitte geben Sie eine Zahl ein");
            }
            else if (String.IsNullOrEmpty(tb_kopfdiameter.Text) & String.IsNullOrEmpty(tb_silinderdiameter.Text) & String.IsNullOrEmpty(tb_silinderhoehe.Text) & String.IsNullOrEmpty(tb_kopfhoehe.Text) & String.IsNullOrEmpty(tb_dichte.Text))
            {
                MessageBox.Show("Bitte geben Sie eine Zahl ein");
            }
            else 
            { */

            try
            { 
                double kopfdiameter = double.Parse(tb_kopfdiameter.Text);
                double silinderdiameter = double.Parse(tb_silinderdiameter.Text);
                double silinderhoehe = double.Parse(tb_silinderhoehe.Text);
                double kopfhoehe = double.Parse(tb_kopfhoehe.Text);
                double dichte = double.Parse(tb_dichte.Text);

                tb_answer1.Text = Convert.ToString(2 * Math.PI * silinderhoehe * Math.Pow(silinderdiameter / 2, 2) + (Math.PI * Math.Pow(kopfdiameter / 2, 2) * kopfhoehe));
                tb_answer2.Text = Convert.ToString((Math.PI * 2 * (silinderdiameter / 2) * ((silinderdiameter / 2) + silinderhoehe)) + (2 * Math.PI * Math.Pow(((Math.Pow((silinderdiameter / 2), 32) + 2 * Math.Pow((kopfdiameter / 2) * kopfhoehe, 16)) / 3), (1 / 16))));
                tb_answer3.Text = Convert.ToString(((2 * Math.PI * silinderhoehe * Math.Pow(silinderdiameter / 2, 2) + (Math.PI * Math.Pow(kopfdiameter / 2, 2))) * kopfhoehe) * (dichte * 0.000001));
            }
            catch (Exception)
            {
                MessageBox.Show("Bitte geben Sie eine Zahl ein");
                tb_kopfdiameter.Clear();
                tb_silinderdiameter.Clear();
                tb_silinderhoehe.Clear();
                tb_kopfhoehe.Clear();
                tb_dichte.Clear();
            }

        }
    }
}
